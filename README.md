# Photo Funnel

Photo Funnel is a simple graphical tool for importing, geotagging, and organizing photos and RAW files on Linux.

<img src="photo-funnel.png" alt="photo-funnel">

# Features

- One-step installation
- Run on any modern Linux distribution
- Easy import of selected files
- Geotag and add a copyright notice to EXIF
- Write camera model, lens, and weather conditions on the day the photo was taken to EXIF

## Dependencies

 - [YAD](https://sourceforge.net/projects/yad-dialog/)
 - [ExifTool](http://www.sno.phy.queensu.ca/~phil/exiftool/)
 - Photo Funnel relies on [Photon](http://photon.komoot.de/) and [Dark Sky](https://darksky.net/) services to obtain geographical coordinates and weather conditions.
 
## Installation

1. Make sure that *curl*, *wget*, *bc*, and *git* tools are installed on your Linux system.

2. Run the `curl -sSL https://is.gd/photo_funnel | bash` command.

## Usage

1. If you see the **Photo Funnel** launcher icon on the Desktop, double-click on the icon to launch the tool. Alternatively, run the `pf` command in the Terminal.
2. During the first run, select a target directory for saving transferred photos and RAW files. When prompted, provide the desired copyright notice.
3. To geotag photos, enter the city where the photos where taken. Leave blank to skip geotagging.
4. Drop the photos and RAW files you want to import onto the **Photo Funnel** drag-and-drop area.
5. Press **Import**.
6. Wait for the *Import completed. Bye!* message.

The tool copies the selected files to the specified target directory. It then obtains the geographical coordinates of the specified city and uses them to geotag the JPEG files. This step is skipped if no city is provided. The tool then renames the files using the *YYYYMMDD-HHMMSS* (e.g., *20170501-135507.jpg*) format and arranges them in folders by date.

## Contributing

1. Fork the repository
2. Create a feature branch: `git checkout -b new-feature`
3. Commit your changes: `git commit -am 'Add a feature'`
4. Push to the branch: `git push origin new-feature`
5. Submit a pull request

## License

[The GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.txt)
