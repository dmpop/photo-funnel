#!/usr/bin/env bash

# Author: Dmitri Popov, dmpop@linux.com
# License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt
# Source code: https://github.com/dmpop/photo-funnel

PREFS="$HOME/.pf"

check=$(wget -q --spider http://photon.komoot.de/)
if [ ! -z $check]; then
  yad --skip-taskbar --undecorated --borders=75 \
      --timeout=3 --no-buttons --center \
      --text="<big>Photon is not reachable. Check your Internet connection.</big>"
  exit 1
fi

if [ ! -f "$PREFS" ]; then
  dir=$(yad --center --width=500 --height=400 --file-selection --title "Select destination directory")
  echo 'dir="'$dir'"' >> "$PREFS"
  echo "Specify copyright notice and press [ENTER]"
  read copyright
  echo 'copyright="'$copyright'"' >> "$PREFS"
fi
source "$PREFS"
f=$(yad --center --text-align=center --title="Photo Funnel" --button=gtk-cancel:0 \
    --button="Import":0 --borders=15 --width=250 --height=150 \
    --text='<big>\nDrop photos and RAW files here.\nPress the <b>Import</b> button.</big>' \
    --dnd | sed 's/^.......//')
city=$(yad --center --width=400 --form --field="Enter city:" | cut -d"|" -f1)

if [ ! -z "$f" ]; then
    if [ ! -d "$dir" ]; then
	mkdir "$dir"
    fi
    cp $f "$dir"
    cd "$dir"
    if [ ! -z "$city" ]; then
	lat=$(curl "photon.komoot.de/api/?q=$city" | jq '.features | .[0] | .geometry | .coordinates | .[1]')
	if (( $(echo "$lat > 0" |bc -l) )); then
            latref="N"
	else
            latref="S"
	fi
	lon=$(curl "photon.komoot.de/api/?q=$city" | jq '.features | .[0] | .geometry | .coordinates | .[0]')
	if (( $(echo "$lon > 0" |bc -l) )); then
            lonref="E"
	else
            lonref="W"
	fi
	exiftool -overwrite_original -copyright="$copyright" -GPSLatitude=$lat -GPSLatitudeRef=$latref -GPSLongitude=$lon -GPSLongitudeRef=$lonref -ext JPG .
	for file in *.JPG
	do
	    camera=$(exiftool -Model $file | cut -d":" -f2 | tr -d " ")
	    lens=$(exiftool -LensID $file | cut -d":" -f2)
	    exiftool -overwrite_original -comment="$camera $lens" "$file"
	done
    fi
    exiftool -d %Y%m%d-%H%M%S%%-c.%%e '-FileName<DateTimeOriginal' .
    exiftool '-Directory<CreateDate' -d ./%Y-%m-%d .
    yad --skip-taskbar --button=gtk-close:0 --center \
	--text="<big>Import completed. Bye!</big>"
else
    yad --skip-taskbar --undecorated --borders=75 \
	--timeout=3 --no-buttons --center \
	--text="<big>Nothing to do. Bye!</big>"
    exit 1
fi
