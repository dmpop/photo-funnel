#!/usr/bin/env bash

# Install dependencies

sudo zypper in yad exiftool

# Clone Photo Funnel Git repository
git clone https://gitlab.com/dmpop/photo-funnel.git
cd photo-funnel

# Install Photo Funnel
cp pf.sh $HOME/bin/pf
sudo cp pf.svg /opt/
cp pf.desktop $HOME/Desktop

yad --skip-taskbar --button=gtk-close:0 --center \
      --text="<big>All done!</big>"
